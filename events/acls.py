import json
import requests
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY


def get_photo(city, state):
    # Use the Pexels API
    url = f"https://api.pexels.com/v1/search?query={city}&{state}/"
    headers = {"AUTHORIZATION": PEXELS_API_KEY}
    r = requests.get(url, headers=headers)
    photos = json.loads(r.text)

    # create dict
    photo = {
        "photo_url": photos["url"]
    }
    return photo
    # Create a dictionary for the headers to use in the request
    # Create the URL for the request with the city and state
    # Make the request
    # Parse the JSON response
    # Return a dictionary that contains a `picture_url` key and
    #   one of the URLs for one of the pictures in the response



# def get_weather_data(city, state):
    # Use the Open Weather API
    # url = f"https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid={OPEN_WEATHER_API_KEY}/"
    # weather = json.loads(response.content)
    # Create the URL for the geocoding API with the city and state
    # Make the request
    # Parse the JSON response
    # Get the latitude and longitude from the response

    # Create the URL for the current weather API with the latitude
    #   and longitude
    # Make the request
    # Parse the JSON response
    # Get the main temperature and the weather's description and put
    #   them in a dictionary
    # Return the dictionary
